﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using RestSharp;

namespace AGLTest
{
    public class Program
    {
        private static void Main(string[] args)
        {
            IList<Person> people = GetPeople();

            IList<string> femaleCats = GetCatNames(people, "Female");

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Female");

            PrintCats(femaleCats);

            IList<string> maleCats = GetCatNames(people, "Male");

            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Male");

            PrintCats(maleCats);

            Console.ReadLine();
        }

        private static IList<Person> GetPeople()
        {
            RestClient client = new RestClient("http://agl-developer-test.azurewebsites.net/");

            RestRequest request = new RestRequest("people.json", Method.GET);

            IRestResponse response = client.Execute(request);

            return JsonConvert.DeserializeObject<IList<Person>>(response.Content);
        }

        private static void PrintCats(IList<string> cats)
        {
            foreach (string cat in cats)
            {
                Console.WriteLine(cat);
            }
        }

        private static IList<string> GetCatNames(IList<Person> people, string gender)
        {
            return people
                .Where(x => x.Pets != null && x.Gender == gender)
                .SelectMany(x => x.Pets.Where(y => y.Type == "Cat"))
                .OrderBy(a => a.Name)
                .Select(z => z.Name).ToList();
        }
    }

    public class Person
    {
        public string Name { get; set; }
        public string Gender { get; set; }
        public int Age { get; set; }
        public IList<Pet> Pets { get; set; }
    }

    public class Pet
    {
        public string Name { get; set; }
        public string Type { get; set; }
    }
}